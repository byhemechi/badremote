const ws = new WebSocket("ws://127.0.0.1:8080");
const cursor = document.querySelector("#cursor");
const c = document.querySelector("#overlay");
const ctx = c.getContext("2d");
const p = {
  x: 0,
  y: 0,
  w: 0,
  h: 0
};

const render = () => {
  c.width = window.innerWidth;
  c.height = window.innerHeight;
  ctx.fillStyle = "#0001"
  ctx.fillRect(p.x, p.y, p.w, p.h);
  requestAnimationFrame(render);
};

requestAnimationFrame(render);

ws.onmessage = m => {
  try {
    const da = JSON.parse(m.data);
    if (da.type === "move") {
      document.querySelectorAll("#body *").forEach(i => i.classList.remove("hover"))
      Object.assign(cursor.style, {
        top: `${da.position.y}px`,
        left: `${da.position.x}px`
      });

      const el = document.elementsFromPoint(da.position.x, da.position.y)[2];
      if (el instanceof HTMLButtonElement || el instanceof HTMLAnchorElement) {
        el.classList.add("hover");
      }

    } else if (da.type === "click") {
      const el = document.elementsFromPoint(da.position.x, da.position.y)[2];
      console.log(el);
      el.click();
    }
  } catch (err) {
    // Yeah nah
    // This is *awful* programming and I do not care
  }
};
