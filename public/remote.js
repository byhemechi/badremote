const ws = new WebSocket("ws://localhost:8080");

document.body.onmousemove = e => ws.send(JSON.stringify({
    type: "move",
    position: {x: e.clientX, y: e.clientY}
}))

document.body.onclick = e => ws.send(JSON.stringify({
    type: "click",
    position: {x: e.clientX, y: e.clientY}
}))